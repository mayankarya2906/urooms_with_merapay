**Project Description**

    In This Team project we have made a website which is used for Room reservation for people who need it.
    Its User-interface is very user-friendly and as its having a nice advanced search engine.
    This project is made using the django-framework and django-orm as the ORM and sqlite3 as the database.
    Search of room and property can be done on the basis of check-in date, check-out date and affordable cost range.Users can reserve the rooms on the basis of this search and also cancel the booking before checkn date
    Besides room reservation, user can also become a partner of uRooms by adding his own property.
    Once the property is added, partner can even make modifications and deleted it if he/she wishes to.

**Pre-requisite**

    We Assume that you already have python3 in your system.
    This project supports django-3.0 version.

**Requirements** 

    The requirements that is there in the requirements.txt file.
    Please run the following command in the project directory.

    pip3 install -r requirements.txt

**Instruction**

    To run the code in your device please follow the below intructions:-
    1.To create virtual environment, run the command: python3 -m venv <name of vitual env>
    2.To activate the enviroment, run the command: source <name of your virtual env>/bin/activate 
    3.To install the requirements run the command: pip3 install -r requirements.txt
    4.To migrate the data from database run:python3 manage.py makemigrations and python3 manage.py migrate
    5.You also need to run the command: python3 manage.py collectstatic 
    (if want to add or delete something into static directory)
    6.To run the server use the command: python3 manage.py runserver (in project root directory)

    Note1:We also make paytm as a payment-gateway in our website by integrating paytm to our website.
    This integration requires Merchant-ID and API key of paytm.
    for this you have to make a paytm merchant account.

    Note2:On the other hand, we also work on DRF(Django Rest Framework) using class-based API with generic API-view which is browser-API view.
    We make API so that our website can be extendable even with APP-development for mobile users.
    As per the reference of the django restframe work we have API documentation attached with project.

    Note3: If you face some error so turn the DEBUG mode = True.


**Directory Structure**

        building_hotel
        ├── about
        │   ├── admin.py
        │   ├── apps.py
        │   ├── __init__.py
        │   ├── migrations
        │   │   ├── 0001_initial.py
        │   │   ├── __init__.py
        │   ├── models.py
        │   ├── templates
        │   │   └── about
        │   │       └── about_us.html
        │   ├── tests.py
        │   ├── urls.py
        │   └── views.py
        ├── accounts
        │   ├── admin.py
        │   ├── apps.py
        │   ├── forms.py
        │   ├── __init__.py
        │   ├── migrations
        │   │   ├── __init__.py
        │   ├── models.py
        │   ├── templates
        │   │   └── accounts
        │   │       ├── login.html
        │   │       └── signup.html
        │   ├── tests.py
        │   ├── urls.py
        │   └── views.py
        ├── agents
        │   ├── admin.py
        │   ├── apps.py
        │   ├── __init__.py
        │   ├── migrations
        │   │   ├── 0001_initial.py
        │   ├── models.py
        │   ├── templates
        │   │   └── agents
        │   │       └── agents.html
        │   ├── tests.py
        │   ├── urls.py
        │   └── views.py
        ├── building_hotel
        │   ├── __init__.py
        │   ├── settings.py
        │   ├── urls.py
        │   └── wsgi.py
        ├── contact
        │   ├── admin.py
        │   ├── apps.py
        │   ├── forms.py
        │   ├── __init__.py
        │   ├── migrations
        │   │   ├── 0001_initial.py
        │   ├── models.py
        │   ├── templates
        │   │   └── contact
        │   │       └── contact.html
        │   ├── tests.py
        │   ├── urls.py
        │   └── views.py
        ├── db.sqlite3
        ├── home
        │   ├── admin.py
        │   ├── apps.py
        │   ├── __init__.py
        │   ├── migrations
        │   │   ├── __init__.py
        │   ├── models.py
        │   ├── templates
        │   │   └── home
        │   │       ├── 404.html
        │   │       ├── 500.html
        │   │       └── home.html
        │   ├── tests.py
        │   ├── urls.py
        │   └── views.py
        ├── manage.py
        ├── media
        │   ├── about
        │   │   ├── 39301735-document-grammar-control-icon.jpg
        │   │   ├── start-real-estate-business-in-Nigeria_hvIZ0Ba_6mPvwVD.jpg
        │   │   ├── start-real-estate-business-in-Nigeria_hvIZ0Ba.jpg
        │   │   └── start-real-estate-business-in-Nigeria.jpg
        │   ├── agents
        │   │   └── 82518477-colorful-cartoon-fat-boy-with-smiling-on-white-background-vector-clipart_peG2ISo.jpg
        │   ├── home
        │   │   ├── bangolre_city.jpg
        │   │   ├── bangolre_city_QofgGwG.jpg
        │   │   ├── Bangolre_G8F7b26.jpeg
        │   │   ├── Bangolre.jpeg
        │   │   ├── Bangolre_MUKrZRe.jpeg
        │   │   ├── Bangolre_p3Z3gyc.jpeg
        │   │   ├── Bangolre_RJe4M9w.jpeg
        │   │   ├── delhi_eFIDXCt.jpg
        │   │   ├── delhi.jpg
        │   │   ├── Hyderabad_APAgYI4.jpg
        │   │   ├── Hyderabad.jpg
        │   │   ├── mumbai.jpg
        │   │   └── mumbai_uETapig.jpg
        │   └── property
        │       ├── 1400987123864.jpeg
        │       ├── 3bed_room_flat_dkqqDej.png
        │       ├── 3bed_room_flat.png
        │       ├── 54850_3971_001.jpg
        │       ├── Bangolre_3801xJs.jpeg
        │       ├── Bangolre_6LNl14p.jpeg
        │       ├── Bangolre_Ajms9DR.jpeg
        │       ├── Bangolre_BkfKMFx.jpeg
        │       ├── bangolre_city_QofgGwG.jpg
        │       ├── Bangolre_D01eC23.jpeg
        │       ├── Bangolre_D9ZX3mX.jpeg
        │       ├── Bangolre_dBwM7Ul.jpeg
        │       ├── Bangolre_iDuUVcy.jpeg
        │       ├── Bangolre_iFPVF6e.jpeg
        │       ├── Bangolre_issQRIH.jpeg
        │       ├── Bangolre.jpeg
        │       ├── Bangolre_nD7Ktip.jpeg
        │       ├── Bangolre_Nmq8fHC.jpeg
        │       ├── Bangolre_T6V566k.jpeg
        │       ├── Bangolre_TmJTlwN.jpeg
        │       ├── Bangolre_TVc562R.jpeg
        │       ├── Bangolre_wIxjpTs.jpeg
        │       ├── Bangolre_ypWBaB8.jpeg
        │       ├── delhi.jpg
        │       ├── hotel2.jpg
        │       ├── Hyderabad_3ti8tuf.jpg
        │       ├── Hyderabad.jpg
        │       ├── image.jpeg
        │       ├── lodge.jpg
        │       ├── lodge_yDfsdGV.jpg
        │       ├── mumbai.jpg
        │       ├── novotel.jpg
        │       ├── resorthyd.jpg
        │       ├── room5.jpg
        │       ├── Screenshot_from_2020-05-10_10-22-17_ekDLY9u.png
        │       └── Screenshot_from_2020-05-10_10-22-17.png
        ├── payment
        │   ├── admin.py
        │   ├── apps.py
        │   ├── __init__.py
        │   ├── migrations
        │   │   ├── 0001_initial.py
        │   ├── models.py
        │   ├── paytm.py
        │   ├── templates
        │   │   └── payment
        │   │       ├── callback.html
        │   │       ├── pay.html
        │   │       └── redirect.html
        │   ├── tests.py
        │   ├── urls.py
        │   └── views.py
        ├── property
        │   ├── admin.py
        │   ├── apps.py
        │   ├── forms.py
        │   ├── __init__.py
        │   ├── migrations
        │   │   ├── 0001_initial.py
        │   │   ├── 0002_auto_20200515_1418.py
        │   │   ├── 0003_auto_20200515_1857.py
        │   │   ├── 0004_auto_20200517_1843.py
        │   │   ├── 0005_auto_20200518_1123.py
        │   ├── models.py
        │   ├── templates
        │   │   └── property
        │   │       ├── add_property.html
        │   │       ├── detail.html
        │   │       ├── invoice.html
        │   │       ├── list.html
        │   │       ├── my_bookings.html
        │   │       ├── my_properties.html
        │   │       └── update_property.html
        │   ├── tests.py
        │   ├── urls.py
        │   ├── utils.py
        │   └── views.py
        ├── rest
        │   ├── admin.py
        │   ├── apps.py
        │   ├── __init__.py
        │   ├── migrations
        │   │   └── __init__.py
        │   ├── models.py
        │   ├── serializer.py
        │   ├── tests.py
        │   ├── urls.py
        │   └── views.py
        ├── static
        │   ├── css
        │   │   ├── custom.css
        │   │   ├── default-skin
        │   │   │   ├── default-skin.css
        │   │   │   ├── default-skin.png
        │   │   │   ├── default-skin.svg
        │   │   │   └── preloader.gif
        │   │   ├── style.css
        │   │   ├── style.min.css
        │   │   ├── styles-merged.css
        │   │   ├── styles-merged.css.map
        │   │   └── vendor
        │   │       ├── animate.css
        │   │       ├── bootstrap.min.css
        │   │       ├── default-skin.css
        │   │       ├── flexslider.css
        │   │       ├── icomoon.css
        │   │       ├── magnific-popup.css
        │   │       ├── owl.carousel.min.css
        │   │       ├── owl.theme.default.min.css
        │   │       └── photoswipe.css
        │   ├── fonts
        │   │   ├── icomoon
        │   │   │   ├── icomoon.eot
        │   │   │   ├── icomoon.svg
        │   │   │   ├── icomoon.ttf
        │   │   │   └── icomoon.woff
        │   │   └── icomoon demo
        │   │       ├── demo-files
        │   │       │   ├── demo.css
        │   │       │   └── demo.js
        │   │       ├── demo.html
        │   │       ├── fonts
        │   │       │   ├── icomoon.eot
        │   │       │   ├── icomoon.svg
        │   │       │   ├── icomoon.ttf
        │   │       │   └── icomoon.woff
        │   │       ├── Read Me.txt
        │   │       ├── selection.json
        │   │       └── style.css
        │   ├── img
        │   │   ├── flaticon
        │   │   │   ├── backup.txt
        │   │   │   ├── license
        │   │   │   │   └── license.pdf
        │   │   │   └── svg
        │   │   │       ├── 001-prize.svg
        │   │   │       ├── 002-camera.svg
        │   │   │       ├── 003-fence.svg
        │   │   │       ├── 004-house.svg
        │   │   │       ├── 005-new.svg
        │   │   │       └── 006-coin.svg
        │   │   ├── loc.png
        │   │   ├── logo.jpg
        │   │   ├── person_1.jpg
        │   │   ├── person_2.jpg
        │   │   ├── person_3.jpg
        │   │   ├── person_4.jpg
        │   │   ├── preloader.gif
        │   │   ├── slide1.jpg
        │   │   ├── slide3.jpeg
        │   │   ├── slide4.jpeg
        │   │   ├── slide5.jpeg
        │   │   ├── slider1.jpg
        │   │   ├── slider2.jpg
        │   │   ├── slider_3.jpg
        │   │   ├── slider_4.jpg
        │   │   ├── slider_5.jpg
        │   │   └── slider.jpg
        │   ├── js
        │   │   ├── custom.js
        │   │   ├── google-map.js
        │   │   ├── main.js
        │   │   ├── main.min.js
        │   │   ├── scripts.js
        │   │   ├── scripts.min.js
        │   │   └── vendor
        │   │       ├── bootstrap.min.js
        │   │       ├── circle-progress.min.js
        │   │       ├── html5shiv.min.js
        │   │       ├── jquery.appear.js
        │   │       ├── jquery.appear.min.js
        │   │       ├── jquery.countTo.js
        │   │       ├── jquery.easing.1.3.js
        │   │       ├── jquery.flexslider-min.js
        │   │       ├── jquery.magnific-popup.min.js
        │   │       ├── jquery.min.js
        │   │       ├── jquery.stellar.min.js
        │   │       ├── jquery.waypoints.min.js
        │   │       ├── owl.carousel.min.js
        │   │       └── respond.min.js
        │   └── scss
        │       ├── _custom-settings.scss
        │       └── style.scss
        ├── staticfiles
        │   ├── admin
        │   │   ├── css
        │   │   │   ├── autocomplete.css
        │   │   │   ├── autocomplete.css.gz
        │   │   │   ├── base.css
        │   │   │   ├── base.css.gz
        │   │   │   ├── changelists.css
        │   │   │   ├── changelists.css.gz
        │   │   │   ├── dashboard.css
        │   │   │   ├── dashboard.css.gz
        │   │   │   ├── fonts.css
        │   │   │   ├── fonts.css.gz
        │   │   │   ├── forms.css
        │   │   │   ├── forms.css.gz
        │   │   │   ├── login.css
        │   │   │   ├── login.css.gz
        │   │   │   ├── responsive.css
        │   │   │   ├── responsive.css.gz
        │   │   │   ├── responsive_rtl.css
        │   │   │   ├── responsive_rtl.css.gz
        │   │   │   ├── rtl.css
        │   │   │   ├── rtl.css.gz
        │   │   │   ├── vendor
        │   │   │   │   └── select2
        │   │   │   │       ├── LICENSE-SELECT2.f94142512c91.md
        │   │   │   │       ├── LICENSE-SELECT2.md
        │   │   │   │       ├── LICENSE-SELECT2.md.gz
        │   │   │   │       ├── select2.css
        │   │   │   │       ├── select2.css.gz
        │   │   │   │       ├── select2.fd9fe49d3d91.css
        │   │   │   │       ├── select2.min.af22a7e2bfec.css
        │   │   │   │       ├── select2.min.css
        │   │   │   │       └── select2.min.css.gz
        │   │   │   ├── widgets.css
        │   │   │   └── widgets.css.gz
        │   │   ├── fonts
        │   │   │   ├── LICENSE.txt
        │   │   │   ├── LICENSE.txt.gz
        │   │   │   ├── README.txt
        │   │   │   ├── README.txt.gz
        │   │   │   ├── Roboto-Bold-webfont.woff
        │   │   │   ├── Roboto-Light-webfont.woff
        │   │   │   └── Roboto-Regular-webfont.woff
        │   │   ├── img
        │   │   │   ├── calendar-icons.svg
        │   │   │   ├── calendar-icons.svg.gz
        │   │   │   ├── gis
        │   │   │   │   ├── move_vertex_off.7a23bf31ef8a.svg
        │   │   │   │   ├── move_vertex_off.svg
        │   │   │   │   ├── move_vertex_off.svg.gz
        │   │   │   │   ├── move_vertex_on.0047eba25b67.svg
        │   │   │   │   ├── move_vertex_on.svg
        │   │   │   │   └── move_vertex_on.svg.gz
        │   │   │   ├── icon-addlink.svg
        │   │   │   ├── icon-addlink.svg.gz
        │   │   │   ├── icon-alert.svg
        │   │   │   ├── icon-alert.svg.gz
        │   │   │   ├── icon-calendar.svg
        │   │   │   ├── icon-calendar.svg.gz
        │   │   │   ├── icon-changelink.svg
        │   │   │   ├── icon-changelink.svg.gz
        │   │   │   ├── icon-clock.svg
        │   │   │   ├── icon-clock.svg.gz
        │   │   │   ├── icon-deletelink.svg
        │   │   │   ├── icon-deletelink.svg.gz
        │   │   │   ├── icon-no.svg
        │   │   │   ├── icon-no.svg.gz
        │   │   │   ├── icon-unknown-alt.svg
        │   │   │   ├── icon-unknown-alt.svg.gz
        │   │   │   ├── icon-unknown.svg
        │   │   │   ├── icon-unknown.svg.gz
        │   │   │   ├── icon-viewlink.svg
        │   │   │   ├── icon-viewlink.svg.gz
        │   │   │   ├── icon-yes.svg
        │   │   │   ├── icon-yes.svg.gz
        │   │   │   ├── inline-delete.svg
        │   │   │   ├── inline-delete.svg.gz
        │   │   │   ├── LICENSE
        │   │   │   ├── LICENSE.gz
        │   │   │   ├── README.txt
        │   │   │   ├── README.txt.gz
        │   │   │   ├── search.svg
        │   │   │   ├── search.svg.gz
        │   │   │   ├── selector-icons.svg
        │   │   │   ├── selector-icons.svg.gz
        │   │   │   ├── sorting-icons.svg
        │   │   │   ├── sorting-icons.svg.gz
        │   │   │   ├── tooltag-add.svg
        │   │   │   ├── tooltag-add.svg.gz
        │   │   │   ├── tooltag-arrowright.svg
        │   │   │   └── tooltag-arrowright.svg.gz
        │   │   └── js
        │   │       ├── actions.js
        │   │       ├── actions.js.gz
        │   │       ├── actions.min.js
        │   │       ├── actions.min.js.gz
        │   │       ├── admin
        │   │       │   ├── DateTimeShortcuts.a9c6d180860b.js
        │   │       │   ├── DateTimeShortcuts.js
        │   │       │   ├── DateTimeShortcuts.js.gz
        │   │       │   ├── RelatedObjectLookups.ea0683bea064.js
        │   │       │   ├── RelatedObjectLookups.js
        │   │       │   └── RelatedObjectLookups.js.gz
        │   │       ├── autocomplete.js
        │   │       ├── autocomplete.js.gz
        │   │       ├── calendar.js
        │   │       ├── calendar.js.gz
        │   │       ├── cancel.js
        │   │       ├── cancel.js.gz
        │   │       ├── change_form.js
        │   │       ├── change_form.js.gz
        │   │       ├── collapse.js
        │   │       ├── collapse.js.gz
        │   │       ├── collapse.min.js
        │   │       ├── collapse.min.js.gz
        │   │       ├── core.js
        │   │       ├── core.js.gz
        │   │       ├── inlines.js
        │   │       ├── inlines.js.gz
        │   │       ├── inlines.min.js
        │   │       ├── inlines.min.js.gz
        │   │       ├── jquery.init.js
        │   │       ├── jquery.init.js.gz
        │   │       ├── popup_response.js
        │   │       ├── popup_response.js.gz
        │   │       ├── prepopulate_init.js
        │   │       ├── prepopulate_init.js.gz
        │   │       ├── prepopulate.js
        │   │       ├── prepopulate.js.gz
        │   │       ├── prepopulate.min.js
        │   │       ├── prepopulate.min.js.gz
        │   │       ├── SelectBox.js
        │   │       ├── SelectBox.js.gz
        │   │       ├── SelectFilter2.js
        │   │       ├── SelectFilter2.js.gz
        │   │       ├── urlify.js
        │   │       ├── urlify.js.gz
        │   │       └── vendor
        │   │           ├── jquery
        │   │           │   ├── jquery.11c05eb286ed.js
        │   │           │   ├── jquery.js
        │   │           │   ├── jquery.js.gz
        │   │           │   ├── jquery.min.220afd743d9e.js
        │   │           │   ├── jquery.min.js
        │   │           │   ├── jquery.min.js.gz
        │   │           │   ├── LICENSE.75308107741f.txt
        │   │           │   ├── LICENSE.txt
        │   │           │   └── LICENSE.txt.gz
        │   │           ├── select2
        │   │           │   ├── i18n
        │   │           │   │   ├── af.c4a5cbd6a23f.js
        │   │           │   │   ├── af.js
        │   │           │   │   ├── af.js.gz
        │   │           │   │   ├── ar.7dcfd5775174.js
        │   │           │   │   ├── ar.js
        │   │           │   │   ├── ar.js.gz
        │   │           │   │   ├── az.1804c238d269.js
        │   │           │   │   ├── az.js
        │   │           │   │   ├── az.js.gz
        │   │           │   │   ├── bg.096f4410173b.js
        │   │           │   │   ├── bg.js
        │   │           │   │   ├── bg.js.gz
        │   │           │   │   ├── bn.b33721dc9b8a.js
        │   │           │   │   ├── bn.js
        │   │           │   │   ├── bn.js.gz
        │   │           │   │   ├── bs.debce43cfca2.js
        │   │           │   │   ├── bs.js
        │   │           │   │   ├── bs.js.gz
        │   │           │   │   ├── ca.60f20182ff18.js
        │   │           │   │   ├── ca.js
        │   │           │   │   ├── ca.js.gz
        │   │           │   │   ├── cs.edd7167cdcb6.js
        │   │           │   │   ├── cs.js
        │   │           │   │   ├── cs.js.gz
        │   │           │   │   ├── da.6bbc262044b3.js
        │   │           │   │   ├── da.js
        │   │           │   │   ├── da.js.gz
        │   │           │   │   ├── de.630e81c65a7b.js
        │   │           │   │   ├── de.js
        │   │           │   │   ├── de.js.gz
        │   │           │   │   ├── dsb.9c2742bfc55a.js
        │   │           │   │   ├── dsb.js
        │   │           │   │   ├── dsb.js.gz
        │   │           │   │   ├── el.01c46bf8c8b3.js
        │   │           │   │   ├── el.js
        │   │           │   │   ├── el.js.gz
        │   │           │   │   ├── en.aed9bad15375.js
        │   │           │   │   ├── en.js
        │   │           │   │   ├── en.js.gz
        │   │           │   │   ├── es.8b21ebdb01ee.js
        │   │           │   │   ├── es.js
        │   │           │   │   ├── es.js.gz
        │   │           │   │   ├── et.32b0b17ba1a9.js
        │   │           │   │   ├── et.js
        │   │           │   │   ├── et.js.gz
        │   │           │   │   ├── eu.6c45eaf416fe.js
        │   │           │   │   ├── eu.js
        │   │           │   │   ├── eu.js.gz
        │   │           │   │   ├── fa.1738b003dd26.js
        │   │           │   │   ├── fa.js
        │   │           │   │   ├── fa.js.gz
        │   │           │   │   ├── fi.2858f3167855.js
        │   │           │   │   ├── fi.js
        │   │           │   │   ├── fi.js.gz
        │   │           │   │   ├── fr.6129248732b9.js
        │   │           │   │   ├── fr.js
        │   │           │   │   ├── fr.js.gz
        │   │           │   │   ├── gl.e2766036e78a.js
        │   │           │   │   ├── gl.js
        │   │           │   │   ├── gl.js.gz
        │   │           │   │   ├── he.4d933538516a.js
        │   │           │   │   ├── he.js
        │   │           │   │   ├── he.js.gz
        │   │           │   │   ├── hi.f81e979ec25f.js
        │   │           │   │   ├── hi.js
        │   │           │   │   ├── hi.js.gz
        │   │           │   │   ├── hr.68583e607f1e.js
        │   │           │   │   ├── hr.js
        │   │           │   │   ├── hr.js.gz
        │   │           │   │   ├── hsb.50caaee90a0d.js
        │   │           │   │   ├── hsb.js
        │   │           │   │   ├── hsb.js.gz
        │   │           │   │   ├── hu.9edad4c24fd0.js
        │   │           │   │   ├── hu.js
        │   │           │   │   ├── hu.js.gz
        │   │           │   │   ├── hy.4c655f53f4e1.js
        │   │           │   │   ├── hy.js
        │   │           │   │   ├── hy.js.gz
        │   │           │   │   ├── id.322604a430a5.js
        │   │           │   │   ├── id.js
        │   │           │   │   ├── id.js.gz
        │   │           │   │   ├── is.a8a13c9122d7.js
        │   │           │   │   ├── is.js
        │   │           │   │   ├── is.js.gz
        │   │           │   │   ├── it.110a0fa84968.js
        │   │           │   │   ├── it.js
        │   │           │   │   ├── it.js.gz
        │   │           │   │   ├── ja.442146837f55.js
        │   │           │   │   ├── ja.js
        │   │           │   │   ├── ja.js.gz
        │   │           │   │   ├── ka.8ea0684cc301.js
        │   │           │   │   ├── ka.js
        │   │           │   │   ├── ka.js.gz
        │   │           │   │   ├── km.8c337905305d.js
        │   │           │   │   ├── km.js
        │   │           │   │   ├── km.js.gz
        │   │           │   │   ├── ko.82358a9b6840.js
        │   │           │   │   ├── ko.js
        │   │           │   │   ├── ko.js.gz
        │   │           │   │   ├── lt.2c390a6bf650.js
        │   │           │   │   ├── lt.js
        │   │           │   │   ├── lt.js.gz
        │   │           │   │   ├── lv.30bfb7fc3b63.js
        │   │           │   │   ├── lv.js
        │   │           │   │   ├── lv.js.gz
        │   │           │   │   ├── mk.92f1d29581b7.js
        │   │           │   │   ├── mk.js
        │   │           │   │   ├── mk.js.gz
        │   │           │   │   ├── ms.ade6aba46542.js
        │   │           │   │   ├── ms.js
        │   │           │   │   ├── ms.js.gz
        │   │           │   │   ├── nb.e535138ca26b.js
        │   │           │   │   ├── nb.js
        │   │           │   │   ├── nb.js.gz
        │   │           │   │   ├── ne.f61bf00bc3fe.js
        │   │           │   │   ├── ne.js
        │   │           │   │   ├── ne.js.gz
        │   │           │   │   ├── nl.674c0d3da68d.js
        │   │           │   │   ├── nl.js
        │   │           │   │   ├── nl.js.gz
        │   │           │   │   ├── pl.a10ee9248c07.js
        │   │           │   │   ├── pl.js
        │   │           │   │   ├── pl.js.gz
        │   │           │   │   ├── ps.de1a40c46c09.js
        │   │           │   │   ├── ps.js
        │   │           │   │   ├── ps.js.gz
        │   │           │   │   ├── pt.5b4ec8cb5b23.js
        │   │           │   │   ├── pt-BR.455adefc2984.js
        │   │           │   │   ├── pt-BR.js
        │   │           │   │   ├── pt-BR.js.gz
        │   │           │   │   ├── pt.js
        │   │           │   │   ├── pt.js.gz
        │   │           │   │   ├── ro.ea7e3b822b06.js
        │   │           │   │   ├── ro.js
        │   │           │   │   ├── ro.js.gz
        │   │           │   │   ├── ru.962f048c22f2.js
        │   │           │   │   ├── ru.js
        │   │           │   │   ├── ru.js.gz
        │   │           │   │   ├── sk.34019208b835.js
        │   │           │   │   ├── sk.js
        │   │           │   │   ├── sk.js.gz
        │   │           │   │   ├── sl.a5e262c643f2.js
        │   │           │   │   ├── sl.js
        │   │           │   │   ├── sl.js.gz
        │   │           │   │   ├── sq.abf2d34b255a.js
        │   │           │   │   ├── sq.js
        │   │           │   │   ├── sq.js.gz
        │   │           │   │   ├── sr.c9f16b9e0f93.js
        │   │           │   │   ├── sr-Cyrl.116365a2de65.js
        │   │           │   │   ├── sr-Cyrl.js
        │   │           │   │   ├── sr-Cyrl.js.gz
        │   │           │   │   ├── sr.js
        │   │           │   │   ├── sr.js.gz
        │   │           │   │   ├── sv.725800c5e8fc.js
        │   │           │   │   ├── sv.js
        │   │           │   │   ├── sv.js.gz
        │   │           │   │   ├── th.b013804dae9c.js
        │   │           │   │   ├── th.js
        │   │           │   │   ├── th.js.gz
        │   │           │   │   ├── tk.5042dc8eca8e.js
        │   │           │   │   ├── tk.js
        │   │           │   │   ├── tk.js.gz
        │   │           │   │   ├── tr.dc697d893beb.js
        │   │           │   │   ├── tr.js
        │   │           │   │   ├── tr.js.gz
        │   │           │   │   ├── uk.e05ad5df6258.js
        │   │           │   │   ├── uk.js
        │   │           │   │   ├── uk.js.gz
        │   │           │   │   ├── vi.0a60056920fc.js
        │   │           │   │   ├── vi.js
        │   │           │   │   ├── vi.js.gz
        │   │           │   │   ├── zh-CN.bde34fa3f064.js
        │   │           │   │   ├── zh-CN.js
        │   │           │   │   ├── zh-CN.js.gz
        │   │           │   │   ├── zh-TW.e727260f7094.js
        │   │           │   │   ├── zh-TW.js
        │   │           │   │   └── zh-TW.js.gz
        │   │           │   ├── LICENSE.f94142512c91.md
        │   │           │   ├── LICENSE.md
        │   │           │   ├── LICENSE.md.gz
        │   │           │   ├── select2.full.d379d5235584.js
        │   │           │   ├── select2.full.js
        │   │           │   ├── select2.full.js.gz
        │   │           │   ├── select2.full.min.68e8d8f673b7.js
        │   │           │   ├── select2.full.min.js
        │   │           │   └── select2.full.min.js.gz
        │   │           └── xregexp
        │   │               ├── LICENSE.d64cecf4f157.txt
        │   │               ├── LICENSE.txt
        │   │               ├── LICENSE.txt.gz
        │   │               ├── xregexp.1865b1cf5085.js
        │   │               ├── xregexp.js
        │   │               ├── xregexp.js.gz
        │   │               ├── xregexp.min.c95393b8ca4d.js
        │   │               ├── xregexp.min.js
        │   │               └── xregexp.min.js.gz
        │   ├── bootstrap_datepicker_plus
        │   │   ├── css
        │   │   │   ├── datepicker-widget.css
        │   │   │   └── datepicker-widget.css.gz
        │   │   └── js
        │   │       ├── datepicker-widget.js
        │   │       └── datepicker-widget.js.gz
        │   ├── css
        │   │   ├── custom.css
        │   │   ├── default-skin
        │   │   │   ├── default-skin.36c613158e16.css
        │   │   │   ├── default-skin.b257fa9c5ac8.svg
        │   │   │   ├── default-skin.css
        │   │   │   ├── default-skin.css.gz
        │   │   │   ├── default-skin.e3f799c6dec9.png
        │   │   │   ├── default-skin.png
        │   │   │   ├── default-skin.svg
        │   │   │   ├── default-skin.svg.gz
        │   │   │   ├── preloader.e34aafbb485a.gif
        │   │   │   └── preloader.gif
        │   │   ├── style.css
        │   │   ├── style.css.gz
        │   │   ├── style.min.css
        │   │   ├── style.min.css.gz
        │   │   ├── styles-merged.css
        │   │   ├── styles-merged.css.gz
        │   │   ├── styles-merged.css.map
        │   │   ├── styles-merged.css.map.gz
        │   │   └── vendor
        │   │       ├── animate.css
        │   │       ├── animate.css.gz
        │   │       ├── bootstrap.min.css
        │   │       ├── bootstrap.min.css.gz
        │   │       ├── default-skin.css
        │   │       ├── default-skin.css.gz
        │   │       ├── flexslider.css
        │   │       ├── flexslider.css.gz
        │   │       ├── icomoon.css
        │   │       ├── icomoon.css.gz
        │   │       ├── magnific-popup.css
        │   │       ├── magnific-popup.css.gz
        │   │       ├── owl.carousel.min.css
        │   │       ├── owl.carousel.min.css.gz
        │   │       ├── owl.theme.default.min.css
        │   │       ├── owl.theme.default.min.css.gz
        │   │       ├── photoswipe.css
        │   │       └── photoswipe.css.gz
        │   ├── fonts
        │   │   ├── icomoon
        │   │   │   ├── icomoon.eot
        │   │   │   ├── icomoon.eot.gz
        │   │   │   ├── icomoon.svg
        │   │   │   ├── icomoon.svg.gz
        │   │   │   ├── icomoon.ttf
        │   │   │   ├── icomoon.ttf.gz
        │   │   │   └── icomoon.woff
        │   │   └── icomoon demo
        │   │       ├── demo-files
        │   │       │   ├── demo.3b9d1a0e781f.js
        │   │       │   ├── demo.a0b54c4a30e4.css
        │   │       │   ├── demo.css
        │   │       │   ├── demo.css.gz
        │   │       │   ├── demo.js
        │   │       │   └── demo.js.gz
        │   │       ├── demo.html
        │   │       ├── demo.html.gz
        │   │       ├── fonts
        │   │       │   ├── icomoon.298566aebf51.svg
        │   │       │   ├── icomoon.7c65c6c2a044.ttf
        │   │       │   ├── icomoon.eot
        │   │       │   ├── icomoon.eot.gz
        │   │       │   ├── icomoon.fb32258d3b4e.eot
        │   │       │   ├── icomoon.fdc5b3418777.woff
        │   │       │   ├── icomoon.svg
        │   │       │   ├── icomoon.svg.gz
        │   │       │   ├── icomoon.ttf
        │   │       │   ├── icomoon.ttf.gz
        │   │       │   └── icomoon.woff
        │   │       ├── Read Me.txt
        │   │       ├── Read Me.txt.gz
        │   │       ├── selection.json
        │   │       ├── selection.json.gz
        │   │       ├── style.css
        │   │       └── style.css.gz
        │   ├── img
        │   │   ├── flaticon
        │   │   │   ├── backup.txt
        │   │   │   ├── backup.txt.gz
        │   │   │   ├── license
        │   │   │   │   ├── license.b01d79588775.pdf
        │   │   │   │   ├── license.pdf
        │   │   │   │   └── license.pdf.gz
        │   │   │   └── svg
        │   │   │       ├── 001-prize.e59ba706c3e9.svg
        │   │   │       ├── 001-prize.svg
        │   │   │       ├── 001-prize.svg.gz
        │   │   │       ├── 002-camera.23b632a1b2c0.svg
        │   │   │       ├── 002-camera.svg
        │   │   │       ├── 002-camera.svg.gz
        │   │   │       ├── 003-fence.cb791f7625fa.svg
        │   │   │       ├── 003-fence.svg
        │   │   │       ├── 003-fence.svg.gz
        │   │   │       ├── 004-house.4cfa35a3f4a8.svg
        │   │   │       ├── 004-house.svg
        │   │   │       ├── 004-house.svg.gz
        │   │   │       ├── 005-new.edd138488d59.svg
        │   │   │       ├── 005-new.svg
        │   │   │       ├── 005-new.svg.gz
        │   │   │       ├── 006-coin.a18480107142.svg
        │   │   │       ├── 006-coin.svg
        │   │   │       └── 006-coin.svg.gz
        │   │   ├── loc.png
        │   │   ├── logo.jpg
        │   │   ├── person_1.jpg
        │   │   ├── person_2.jpg
        │   │   ├── person_3.jpg
        │   │   ├── person_4.jpg
        │   │   ├── preloader.gif
        │   │   ├── slide1.jpg
        │   │   ├── slide3.jpeg
        │   │   ├── slide4.jpeg
        │   │   ├── slide5.jpeg
        │   │   ├── slider_1.jpg
        │   │   ├── slider1.jpg
        │   │   ├── slider_2.jpg
        │   │   ├── slider2.jpg
        │   │   ├── slider_3.jpg
        │   │   ├── slider_4.jpg
        │   │   ├── slider_5.jpg
        │   │   └── slider.jpg
        │   ├── js
        │   │   ├── custom.js
        │   │   ├── google-map.js
        │   │   ├── google-map.js.gz
        │   │   ├── main.js
        │   │   ├── main.js.gz
        │   │   ├── main.min.js
        │   │   ├── main.min.js.gz
        │   │   ├── scripts.js
        │   │   ├── scripts.js.gz
        │   │   ├── scripts.min.js
        │   │   ├── scripts.min.js.gz
        │   │   └── vendor
        │   │       ├── bootstrap.min.js
        │   │       ├── bootstrap.min.js.gz
        │   │       ├── circle-progress.min.js
        │   │       ├── circle-progress.min.js.gz
        │   │       ├── html5shiv.min.js
        │   │       ├── html5shiv.min.js.gz
        │   │       ├── jquery.appear.js
        │   │       ├── jquery.appear.js.gz
        │   │       ├── jquery.appear.min.js
        │   │       ├── jquery.appear.min.js.gz
        │   │       ├── jquery.countTo.js
        │   │       ├── jquery.countTo.js.gz
        │   │       ├── jquery.easing.1.3.js
        │   │       ├── jquery.easing.1.3.js.gz
        │   │       ├── jquery.flexslider-min.js
        │   │       ├── jquery.flexslider-min.js.gz
        │   │       ├── jquery.magnific-popup.min.js
        │   │       ├── jquery.magnific-popup.min.js.gz
        │   │       ├── jquery.min.js
        │   │       ├── jquery.min.js.gz
        │   │       ├── jquery.stellar.min.js
        │   │       ├── jquery.stellar.min.js.gz
        │   │       ├── jquery.waypoints.min.js
        │   │       ├── jquery.waypoints.min.js.gz
        │   │       ├── owl.carousel.min.js
        │   │       ├── owl.carousel.min.js.gz
        │   │       ├── respond.min.js
        │   │       └── respond.min.js.gz
        │   ├── rest_framework
        │   │   ├── css
        │   │   │   ├── bootstrap.min.css
        │   │   │   ├── bootstrap.min.css.gz
        │   │   │   ├── bootstrap-theme.min.css
        │   │   │   ├── bootstrap-theme.min.css.gz
        │   │   │   ├── bootstrap-tweaks.css
        │   │   │   ├── bootstrap-tweaks.css.gz
        │   │   │   ├── default.css
        │   │   │   ├── default.css.gz
        │   │   │   ├── font-awesome-4.0.3.css
        │   │   │   ├── font-awesome-4.0.3.css.gz
        │   │   │   ├── prettify.css
        │   │   │   └── prettify.css.gz
        │   │   ├── docs
        │   │   │   ├── css
        │   │   │   │   ├── base.css
        │   │   │   │   ├── base.css.gz
        │   │   │   │   ├── base.d9cd17260641.css
        │   │   │   │   ├── highlight.css
        │   │   │   │   ├── highlight.css.gz
        │   │   │   │   ├── highlight.e0e4d973c6d7.css
        │   │   │   │   ├── jquery.json-view.min.a2e6beeb6710.css
        │   │   │   │   ├── jquery.json-view.min.css
        │   │   │   │   └── jquery.json-view.min.css.gz
        │   │   │   ├── img
        │   │   │   │   ├── favicon.5195b4d0f3eb.ico
        │   │   │   │   ├── favicon.ico
        │   │   │   │   ├── favicon.ico.gz
        │   │   │   │   ├── grid.a4b938cf382b.png
        │   │   │   │   └── grid.png
        │   │   │   └── js
        │   │   │       ├── api.c9743eab7a4f.js
        │   │   │       ├── api.js
        │   │   │       ├── api.js.gz
        │   │   │       ├── highlight.pack.479b5f21dcba.js
        │   │   │       ├── highlight.pack.js
        │   │   │       ├── highlight.pack.js.gz
        │   │   │       ├── jquery.json-view.min.b7c2d6981377.js
        │   │   │       ├── jquery.json-view.min.js
        │   │   │       └── jquery.json-view.min.js.gz
        │   │   ├── fonts
        │   │   │   ├── fontawesome-webfont.eot
        │   │   │   ├── fontawesome-webfont.svg
        │   │   │   ├── fontawesome-webfont.svg.gz
        │   │   │   ├── fontawesome-webfont.ttf
        │   │   │   ├── fontawesome-webfont.ttf.gz
        │   │   │   ├── fontawesome-webfont.woff
        │   │   │   ├── glyphicons-halflings-regular.eot
        │   │   │   ├── glyphicons-halflings-regular.svg
        │   │   │   ├── glyphicons-halflings-regular.svg.gz
        │   │   │   ├── glyphicons-halflings-regular.ttf
        │   │   │   ├── glyphicons-halflings-regular.ttf.gz
        │   │   │   ├── glyphicons-halflings-regular.woff
        │   │   │   └── glyphicons-halflings-regular.woff2
        │   │   ├── img
        │   │   │   ├── glyphicons-halflings.png
        │   │   │   ├── glyphicons-halflings-white.png
        │   │   │   └── grid.png
        │   │   └── js
        │   │       ├── ajax-form.js
        │   │       ├── ajax-form.js.gz
        │   │       ├── bootstrap.min.js
        │   │       ├── bootstrap.min.js.gz
        │   │       ├── coreapi-0.1.1.js
        │   │       ├── coreapi-0.1.1.js.gz
        │   │       ├── csrf.js
        │   │       ├── csrf.js.gz
        │   │       ├── default.js
        │   │       ├── default.js.gz
        │   │       ├── jquery-3.3.1.min.js
        │   │       ├── jquery-3.3.1.min.js.gz
        │   │       ├── jquery-3.4.1.min.js
        │   │       ├── jquery-3.4.1.min.js.gz
        │   │       ├── prettify-min.js
        │   │       └── prettify-min.js.gz
        │   └── scss
        │       ├── _custom-settings.scss
        │       ├── _custom-settings.scss.gz
        │       ├── style.scss
        │       └── style.scss.gz
        └── templates
            └── base.html
