from django.db import models
from django.contrib.auth import get_user_model
from property.models import Property


User = get_user_model()

payment_type = {('wallet', 'wallet'),
                ('debit', 'debit'),
                ('credit', 'credit')}


# class Transaction(models.Model):
#     made_by = models.ForeignKey(User, related_name='transactions',
#                                 on_delete=models.CASCADE)
#     made_on = models.DateTimeField(auto_now_add=True)
#     amount = models.IntegerField()
#     order_id = models.CharField(unique=True, max_length=100, null=True, blank=True)
#     checksum = models.CharField(max_length=100, null=True, blank=True)

#     def save(self, *args, **kwargs):
#         if self.order_id is None and self.made_on and self.id:
#             self.order_id = self.made_on.strftime('PAY2ME%Y%m%dODR') + str(self.id)
#         return super().save(*args, **kwargs)


class Payment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    prop = models.ForeignKey(Property, on_delete=models.CASCADE,
                             related_name='payment')
    ref_id = models.PositiveIntegerField(null=True)
    service_name = models.CharField(default='Room_Reservation', max_length=50)
    service_owner = models.CharField(max_length=50)
    service_price = models.PositiveIntegerField(default=10)
    payment_type = models.CharField(choices=payment_type, max_length=50)
    status = models.CharField(default='Fail', max_length=50)

    class Meta:

        def __str__(self):
            return str(self.ref_id)
