from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm
from .forms import UserCreationForm
from django.contrib.auth import login, logout


def signup_view(request):

    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            #  log the user in
            return redirect('home:home')
    else:
        form = UserCreationForm()
    context = {'form': form}

    return render(request, 'accounts/signup.html', context)


def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            # log the user in
            user = form.get_user()
            login(request, user)
            if 'next' in request.POST:
                return redirect(request.POST.get('next'))
            else:
                return redirect('home:home')
    else:
        form = AuthenticationForm()
    context = {'form': form}

    return render(request, 'accounts/login.html', context)


def logout_view(request):
    logout(request)
    return render(request, 'home/home.html')
