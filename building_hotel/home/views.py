from django.shortcuts import render
from property.models import Property, Category


def home(request):
    if request.method == 'POST':
        recived_data = dict(request.POST)
        print(recived_data)
    else:
        print(dict(request.GET))
    category_list = Category.objects.all()
    property_list = Property.objects.all()
    templates = 'home/home.html'
    context = {'category_list_home': category_list,
               'property_list_home': property_list}
    return render(request, templates, context)


def error_404_view(request, exception):
    templates = 'home/404.html'
    return render(request, templates)


def error_500_view(request, *args, **argv):
    templates = 'home/500.html'
    return render(request, templates)
