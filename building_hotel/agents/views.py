from django.shortcuts import render
from .models import Agents
from django.contrib.auth.decorators import login_required


@login_required(login_url="/accounts/login/")
def agents_list(request):
    agents_list = Agents.objects.all()
    templates = 'agents/agents.html'
    context = {'agents_list': agents_list}
    return render(request, templates, context)
