from django.db import models


class Agents(models.Model):
    name = models.CharField(max_length=50)
    title = models.CharField(max_length=50)
    email = models.CharField(max_length=100)
    image = models.ImageField(upload_to='agents/', null=True)

    def __str__(self):
        return self.name
