from django.shortcuts import render
from .models import About

# Create your views here.
from django.contrib.auth.decorators import login_required


@login_required(login_url="/accounts/login/")
def about_us(request):
    about_us = About.objects.first()
    templates = 'about/about_us.html'
    context = {'about': about_us}

    return render(request, templates, context)
