from django.shortcuts import render, redirect, get_object_or_404
from payment.models import Payment
from django.db.models import Sum
from django.contrib import messages
# from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
# from payment.paytm import generate_checksum, verify_checksum
from .models import Property, Category, Room, Reservation, Rating
from .forms import ReserveForm, AddPropertyForm, AddRoomForm, AddRating
from payment.forms import PaymentForm
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from datetime import date
import datetime
from django.http import HttpResponse
from django.views import View
from .utils import render_to_pdf
from django.contrib.auth import get_user_model
from datetime import timedelta

User = get_user_model()
reservation = Reservation.objects.none()
total_price = 0


@login_required(login_url="/accounts/login/")
def property_list(request):
    property_list = Property.objects.all().order_by('-average_rating')
    room_list = Room.objects.all()
    templates = 'property/list.html'
    if request.method == 'POST':
        location_query = request.POST.get('q')
        property_type = request.POST.get('property_type')
        range_date = request.POST.get("daterange")
        min_cost = request.POST.get("min_cost")
        max_cost = request.POST.get("max_cost")
        Checkin = date.today()
        Checkout = Checkin + timedelta(days=1)
        if range_date:
            dates = range_date.split(' to ')
            FirstDate = dates[0]
            SecDate = dates[1]

            if FirstDate and SecDate:
                Checkin = datetime.datetime.strptime(
                    FirstDate, "%Y-%m-%d").date()
                Checkout = datetime.datetime.strptime(
                    SecDate, "%Y-%m-%d").date()

        if property_type and location_query:
            if property_type != "All":
                property_list = property_list.filter(
                    Q(property_type__icontains=property_type)
                    & Q(location__icontains=location_query))
            else:
                property_list = property_list.filter(
                    location__icontains=location_query)
        elif property_type and not location_query:
            if property_type != "All":
                property_list = property_list.filter(
                    property_type__icontains=property_type)
                print(property_list)

        elif location_query and not property_type:
            property_list = property_list.filter(
                location__icontains=location_query)
        else:
            property_list = Property.objects.all()
            print(property_list)

        property_all_list = []
        for property in property_list:
            rooms_booked = Reservation.objects.filter(prop=property).filter(
                Q(CheckIn__lte=Checkout) & Q(CheckOut__gte=Checkin))
            count_booked_rooms = rooms_booked.count()

            room = Room.objects.filter(prop=property).last()

            if room:
                total_rooms = room.total_rooms
                rooms_available = total_rooms - count_booked_rooms

                if rooms_available:
                    if max_cost and min_cost:
                        if (room.price_per_night >= int(min_cost)
                           and room.price_per_night <= int(max_cost)):
                            property_all_list.append(property)
                    elif min_cost and not max_cost:
                        if room.price_per_night >= int(min_cost):
                            property_all_list.append(property)

                    elif not min_cost and max_cost:
                        if room.price_per_night <= int(max_cost):
                            property_all_list.append(property)
                    else:
                        property_all_list.append(property)

        context = {'property_list': property_all_list, 'room_list': room_list}

        return render(request, templates, context)

    context = {'property_list': property_list, 'room_list': room_list}

    return render(request, templates, context)


@login_required(login_url="/accounts/login/")
def property_detail(request, id):
    property_detail = Property.objects.get(id=id)
    user = User.objects.get(id=request.user.id)
    room = Room.objects.filter(prop=property_detail).last()
    reserve_form = Reservation.objects.none()
    if room:
        room_price = room.price_per_night
        if request.method == 'POST':
            reserve_form = ReserveForm(request.POST)
            if reserve_form.is_valid():
                global reservation
                reservation = reserve_form.save(commit=False)
                reservation.user = request.user
                reservation.prop = property_detail
                checkin_date = reservation.CheckIn
                checkout_date = reservation.CheckOut
                rooms_booked = Reservation.objects.filter(
                    prop=property_detail).filter(
                        Q(CheckIn__lte=checkout_date)
                        & Q(CheckOut__gte=checkin_date))
                count = rooms_booked.count()
                count = int(count)
                rooms_available = room.total_rooms
                rooms_available = int(rooms_available)
                rooms_left = rooms_available - count

                if rooms_left:
                    delta = checkout_date - checkin_date
                    days = delta.days
                    global total_price
                    total_price = int(days)*room_price
                    reservation.totalPrice = total_price
                    return redirect('property:payment_detail', id=property_detail.id)

                else:
                    reserve_form = ReserveForm()
                    messages.success(request, 'rooms are not available')
                    return render(request, 'property/detail.html',
                                  context={'property_detail': property_detail,
                                           'reserve_form': reserve_form,
                                           'room_detail': room,
                                           'date': datetime.date.today()})
        else:
            reserve_form = ReserveForm()

    templates = 'property/detail.html'

    context = {'property_detail': property_detail, 'room_detail': room,
               'reserve_form': reserve_form,
               'date': datetime.date.today()}

    return render(request, templates, context=context)


@csrf_exempt
def callback(request):
    print("method", request.method)
    print("nothing", request.data)
    if request.method == 'POST':
        # print(request.POST['data'])
        received_data = request.data
        print("post data",received_data)
    else:
        print("ge data",request.data)
    return render(request, 'payment/callback.html', context=received_data)

@login_required(login_url="/accounts/login/")
def payment_detail(request, id):
    property_detail = Property.objects.get(id=id)
    if request.method == 'POST':
        payment_form = PaymentForm(request.POST)
        if payment_form.is_valid():
            payment_method = payment_form.save(commit=False)
            print(total_price)
            payment_method.service_price = total_price
            payment_method.user = request.user
            payment_method.prop = property_detail
            payment_method.save()
            return redirect('property:payment_link', id=property_detail.id)
    else:
        payment_form = PaymentForm()

    return render(request, 'property/payment_detail.html', context={'payment_form': payment_form})


@login_required(login_url="/accounts/login/")
def payment_link(request, id):
    property_detail = Property.objects.get(id=id)
    payment_link = Payment.objects.filter(Q(prop=property_detail) & Q(user=request.user)).last()
    context = {'payment_link': payment_link}
    templates = 'property/link.html'
    return render(request, templates, context)


@login_required(login_url="/accounts/login/")
def want_to_add_property(request):
    templates = 'property/want_to_add_property.html'
    return render(request, templates)


@login_required(login_url="/accounts/login/")
def add_property(request):
    templates = 'property/add_property.html'
    if request.method == 'POST':
        add_property_form = AddPropertyForm(request.POST, request.FILES)
        add_room_form = AddRoomForm(request.POST, request.FILES)
        if add_property_form.is_valid() and add_room_form.is_valid():
            add_assets = add_property_form.save(commit=False)
            add_assets_room = add_room_form.save(commit=False)
            add_assets.user = request.user
            add_assets.date_of_add = date.today()
            add_assets.save()
            property_instance = Property.objects.get(
                                property_name=add_assets.property_name)
            add_assets_room.prop = property_instance
            add_room_form.save()

            return redirect('property:my_property')

    else:
        add_property_form = AddPropertyForm()
        add_room_form = AddRoomForm()

    context = {'add_property_form': add_property_form,
               'add_room_form': add_room_form}

    return render(request, templates, context)


@login_required(login_url="/accounts/login/")
def update_property(request, id=None):
    obj_prop = get_object_or_404(Property, id=id)
    room_instance = Room.objects.filter(prop=obj_prop).last()
    if room_instance:
        obj_room = get_object_or_404(Room, id=room_instance.id)
        add_room_form = AddRoomForm(request.POST or None, instance=obj_room)
    else:
        add_room_form = AddRoomForm(request.POST or None,
                                    instance=room_instance)

    add_property_form = AddPropertyForm(request.POST or None,
                                        request.FILES or None,
                                        instance=obj_prop)

    if request.method == 'POST':
        if add_property_form.is_valid() and add_room_form.is_valid():
            add_assets = add_property_form.save(commit=False)
            add_assets_room = add_room_form.save(commit=False)
            add_assets.user = request.user
            add_assets.date_of_add = date.today()
            add_assets.save()
            property_instance = Property.objects.get(
                                property_name=add_assets.property_name)
            add_assets_room.prop = property_instance
            add_room_form.save()
            return redirect('property:my_property')

    context = {'add_property_form': add_property_form,
               'add_room_form': add_room_form}

    return render(request, 'property/update_property.html', context=context)


def property_by_category(request, id):
    category = Category.objects.get(id=id)
    property_list = Property.objects.all().filter(category=category)
    room_list = []
    for property in property_list:
        room_list.append(Room.objects.filter(prop=property).last())

    context = {'property_list': property_list, 'room_list': room_list}
    templates = 'property/list.html'
    return render(request, templates, context)


@login_required(login_url="/accounts/login/")
def add_rating(request, id):
    templates = 'property/rating.html'
    property_instance = Property.objects.get(id=id)
    rating_instance = Rating.objects.filter(Q(prop=property_instance)
                                            & Q(user=request.user))

    rating_user = Rating.objects.filter(Q(prop=property_instance)
                                        & Q(user=request.user)).last()
    count = rating_instance.count()
    if request.method == 'POST':
        add_rating_form = AddRating(request.POST)
        if add_rating_form.is_valid():
            rating_add = add_rating_form.save(commit=False)
            rating_add.created_at = datetime.date.today()
            rating_add.user = request.user
            rating_add.prop = property_instance
            rating_add.save()
            no_of_ratings = Rating.objects.filter(prop=property_instance).count()
            sum_rating = Rating.objects.filter(prop=property_instance).aggregate(sum=Sum('rating_choice'))['sum']
            avg_rating = round(sum_rating/no_of_ratings, 1)
            property_instance.average_rating = avg_rating
            property_instance.save(update_fields=['average_rating'])
            return redirect('property:my_bookings')
    else:
        add_rating_form = AddRating()
    context = {'add_rating_form': add_rating_form,
               'count': count, 'rating_user': rating_user}
    return render(request, templates, context)


@login_required(login_url="/accounts/login/")
def update_rating(request, id=None):
    obj_rating = get_object_or_404(Rating, id=id)
    add_rating_form = AddRating(request.POST or None,
                                instance=obj_rating)
    property_instance = obj_rating.prop

    if request.method == 'POST':
        if add_rating_form.is_valid():
            rating_add = add_rating_form.save(commit=False)
            rating_add.created_at = datetime.date.today()
            rating_add.user = request.user
            rating_add.prop = property_instance
            rating_add.save()
            no_of_ratings = Rating.objects.filter(prop=property_instance).count()
            sum_rating = Rating.objects.filter(prop=property_instance).aggregate(sum=Sum('rating_choice'))['sum']
            avg_rating = round(sum_rating/no_of_ratings, 1)
            property_instance.average_rating = avg_rating
            property_instance.save(update_fields=['average_rating'])
            return redirect('property:my_bookings')
        else:
            add_rating_form = AddRating()
            return redirect('property:my_property')

    context = {'add_rating_form': add_rating_form}

    return render(request, 'property/update_rating.html', context=context)


@login_required(login_url="/accounts/login/")
def delete_rating(request, id):
    my_rating = Rating.objects.get(id=id)
    my_rating.delete()
    return redirect('property:my_bookings')


@login_required(login_url="/accounts/login/")
def my_bookings(request):
    bookings = Reservation.objects.filter(user=request.user)
    context = {'bookings': bookings, 'date': datetime.date.today()}
    return render(request, 'property/my_bookings.html', context)


@login_required(login_url="/accounts/login/")
def cancelbooking(request, id):
    booking = Reservation.objects.get(id=id)
    booking.delete()
    return redirect('property:my_bookings')


@login_required(login_url="/accounts/login/")
def my_property(request):
    my_properties = Property.objects.filter(user=request.user)
    room_list = Room.objects.all()
    context = {'my_properties': my_properties, 'room_list': room_list}
    return render(request, 'property/my_properties.html', context)


@login_required(login_url="/accounts/login/")
def delete_property(request, id):
    my_property = Property.objects.get(id=id)
    my_property.delete()
    return redirect('property:my_property')


class GeneratePDF(View):
    def get(self, request, *args, **kwargs):
        booking = Reservation.objects.get(id=self.kwargs['id'])
        context = {"booking": booking}
        pdf = render_to_pdf('property/invoice.html', context)
        return HttpResponse(pdf, content_type='application/pdf')
