from django.urls import path
from . import views


app_name = 'property'

urlpatterns = [
    path('', views.property_list, name="property_list"),
    path('<int:id>', views.property_detail, name="property_detail"),
    path('category/<int:id>/', views.property_by_category,
         name='property_by_category'),
    path('adding/', views.want_to_add_property, name='want_to_add_property'),
    path('add/', views.add_property, name="add_property"),
    path('update/<int:id>/', views.update_property, name='update_property'),
    path('callback/', views.callback, name='callback'),
    path('bookings/', views.my_bookings, name='my_bookings'),
    path('cancel/bookings/<int:id>/', views.cancelbooking,
         name='cancelbooking'),
    path('my_property/', views.my_property, name='my_property'),
    path('delete/property/<int:id>/', views.delete_property,
         name='delete_property'),
    path('bookings/<int:id>/pdf', views.GeneratePDF.as_view(), name='gpdf'),
    path('add/rating/property/<int:id>/', views.add_rating, name='add_rating'),
    path('update/rating/<int:id>/', views.update_rating, name='update_rating'),
    path('delete/rating/<int:id>/', views.delete_rating, name='delete_rating'),
    path('<int:id>/pay/', views.payment_detail, name='payment_detail'),
    path('<int:id>/pay/link/', views.payment_link, name='payment_link'),


]
