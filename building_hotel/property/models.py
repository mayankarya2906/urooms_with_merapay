from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()


property_type = {
    ('hotel', 'hotel'),
    ('resort', 'resort'),

}

beds_number = {
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
}

rating_choice = {
    ('1', '1'),
    ('2','2'),
    ('3', '3'),
    ('4', '4'),
    ('5', '5')
}


class Property(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    category = models.ForeignKey('Category', on_delete=models.CASCADE,
                                 related_name="property_category")
    property_type = models.CharField(choices=property_type, max_length=50)
    property_name = models.CharField(max_length=100)
    image = models.FileField(upload_to='property/')
    average_rating = models.DecimalField(decimal_places=1, max_digits=2, default=0.0)
    location = models.CharField(max_length=300)
    date_of_add = models.DateField()
    parking_facility = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Property'
        verbose_name_plural = 'Properties'

    def __str__(self):
        return self.property_name


class Category(models.Model):
    category_name = models.CharField(max_length=30, null=True)
    image = models.ImageField(upload_to='home/', null=True)

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.category_name


class Room(models.Model):
    prop = models.ForeignKey('Property', on_delete=models.CASCADE,
                             related_name='room')
    total_rooms = models.PositiveIntegerField()
    area_per_m2 = models.DecimalField(decimal_places=2, max_digits=8)
    beds_number = models.CharField(choices=beds_number, max_length=50)
    price_per_night = models.PositiveIntegerField()

    class Meta:
        verbose_name = 'Room'
        verbose_name_plural = 'Rooms'

    def __str__(self):
        return str(self.id)


class Reservation(models.Model):
    prop = models.ForeignKey('Property', on_delete=models.CASCADE,
                             related_name="properties")
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    guestFirstName = models.CharField(max_length=255)
    guestLastName = models.CharField(max_length=255)
    CheckIn = models.DateField()
    CheckOut = models.DateField()
    totalPrice = models.PositiveIntegerField(default=0)

    class Meta:
        verbose_name_plural = 'Reservation'

    def __str__(self):
        return self.guestLastName


class Rating(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    prop = models.ForeignKey('Property', on_delete=models.CASCADE,
                             related_name="prop_rating")
    rating_choice = models.CharField(choices=rating_choice, max_length=50, null=True)
    created_at = models.DateTimeField(null=True)

    class Meta:
        verbose_name_plural = 'Rating'

    def __str__(self):
        return self.rating_choice
