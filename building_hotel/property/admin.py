from django.contrib import admin
from .models import Property, Room, Reservation, Category

admin.site.register(Property)
admin.site.register(Room)
admin.site.register(Reservation)
admin.site.register(Category)





# Register your models here.
