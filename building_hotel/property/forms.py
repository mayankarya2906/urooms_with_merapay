from django import forms
from .models import Reservation, Property, Room, Rating
import datetime
from django.core.exceptions import ValidationError


checkin_date = datetime.date.today()


class ReserveForm(forms.ModelForm):
    def clean_CheckIn(self):
        global checkin_date
        checkin_date = self.cleaned_data['CheckIn']

        if checkin_date < datetime.date.today():
            raise ValidationError('Invalid date')
        return checkin_date

    def clean_CheckOut(self):
        checkout_date = self.cleaned_data['CheckOut']

        if checkout_date <= checkin_date:
            raise ValidationError('Please check checkout date')

        return checkout_date

    class Meta:
        model = Reservation
        widgets = {'CheckIn': forms.DateInput(attrs={'class': 'datepicker',
                                                     'type': 'date'}),
        'CheckOut': forms.DateInput(attrs={'class': 'datepicker', 'type': 'date'})
        }

        exclude = ['totalPrice', 'hotel', 'user', 'prop']


class AddPropertyForm(forms.ModelForm):
    class Meta:
        model = Property
        exclude = ['date_of_add', 'user', 'average_rating']


class AddRoomForm(forms.ModelForm):
    class Meta:
        model = Room
        exclude = ['prop']


class AddRating(forms.ModelForm):
    class Meta:
        model = Rating
        exclude = ['created_at', 'user', 'prop']

