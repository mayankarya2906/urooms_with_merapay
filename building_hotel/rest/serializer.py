from property.models import Property, Reservation, Room
from rest_framework import serializers
import datetime
from datetime import timedelta
from django.db.models import Q
from django.contrib.auth import get_user_model


User = get_user_model()


class UserSerializer(serializers.ModelSerializer):

    password = serializers.CharField(write_only=True)

    def create(self, validated_data):

        user = User.objects.create(
            username=validated_data['username'],
        )
        user.set_password(validated_data['password'])
        user.save()

        return user

    class Meta:
        model = User
        fields = ["id", "username", "password"]


class PropertySerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=None)
    date_of_add = serializers.ReadOnlyField()

    class Meta:
        model = Property
        exclude = ['average_rating']

    def create(self, data):
        data['user'] = self.context['request'].user.id
        data['user'] = User.objects.get(id=data['user'])
        data['date_of_add'] = datetime.date.today()
        property = super(PropertySerializer, self).create(data)
        return property

    def update(self, instance, data):
        instance.category = data.get('category', instance.category)
        instance.property_type = data.get('property_type',
                                          instance.property_type)
        instance.property_name = data.get('property_name',
                                          instance.property_name)
        instance.location = data.get('location', instance.location)
        instance.date_of_add = data.get('date_of_add', instance.date_of_add)
        instance.parking_facility = data.get('parking_facility',
                                             instance.parking_facility)
        instance.save()

        return instance


class ReservationSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=None)
    prop_id = serializers.ReadOnlyField()

    def validate(self, data):

        prop_id = int(self.context['request'].parser_context['kwargs']['id'])
        prop_instance = Property.objects.get(id=prop_id)

        room_instance = Room.objects.filter(prop=prop_instance).last()
        if room_instance:
            checkin_date = data["CheckIn"]
            checkout_date = data["CheckOut"]
            rooms_booked = Reservation.objects.filter(
                prop=prop_instance).filter(Q(CheckIn__lte=checkout_date)
                                           & Q(CheckOut__gte=checkin_date))
            count = rooms_booked.count()
            count = int(count)
            rooms_available = room_instance.total_rooms
            rooms_available = int(rooms_available)
            rooms_left = rooms_available - count

        else:
            raise serializers.ValidationError({
                "Message": "Room is not added by owner"
            })

        if not rooms_left:
            raise serializers.ValidationError({
                "Message":
                "Room is not available during this checkin and checkout date"
            })

        if data["CheckIn"] < datetime.date.today():
            raise serializers.ValidationError({
                "Invalid Date": "CheckIn date should be greater than today"
            })

        elif data["CheckIn"] > datetime.date.today() + timedelta(days=90):

            raise serializers.ValidationError({
                "Invalid Checkout Date":
                "CheckIn date should be within 90 days from today"
            })

        if data["CheckOut"] > data["CheckIn"] + datetime.timedelta(days=90):

            raise serializers.ValidationError({
                "Invalid Checkout Date":
                "Reservation can't be done for more than 90 days"
            })

        if data["CheckIn"] > data["CheckOut"]:
            raise serializers.ValidationError({
                "Invalid date":
                "Checkout date must be greater than checkin date"
            })
        return super(ReservationSerializer, self).validate(data)

    class Meta:
        model = Reservation
        fields = ['id', 'user', 'guestFirstName', 'guestLastName', 'CheckIn',
                  'CheckOut', 'prop_id']

    def create(self, validate_data):
        validate_data['user'] = self.context['request'].user.id
        validate_data['user'] = User.objects.get(id=validate_data['user'])
        prop_id = self.context['request'].parser_context.get('kwargs')['id']
        validate_data['prop'] = Property.objects.get(id=prop_id)
        reservation = super(ReservationSerializer, self).create(validate_data)
        return reservation

    def update(self, instance, data):
        instance.prop = data.get('prop', instance.prop)
        instance.guestFirstName = data.get('guestFirstName',
                                           instance.guestFirstName)
        instance.guestLastName = data.get('guestLastName',
                                          instance.guestLastName)
        instance.CheckIn = data.get('CheckIn', instance.CheckIn)
        instance.CheckOut = data.get('CheckOut', instance.CheckOut)
        instance.totalPrice = data.get('totalPrice', instance.totalPrice)
        instance.save()
        return instance


class RoomSerializer(serializers.ModelSerializer):
    prop_id = serializers.ReadOnlyField()

    class Meta:
        model = Room
        fields = ['id', 'prop_id', 'total_rooms', 'area_per_m2',
                  'beds_number', 'price_per_night']

    def create(self, validate_data):
        prop_id = self.context['request'].parser_context.get('kwargs')['id']
        validate_data['prop'] = Property.objects.get(id=prop_id)
        room = super(RoomSerializer, self).create(validate_data)
        return room
