from property.models import Property, Reservation, Room
from .serializer import PropertySerializer, ReservationSerializer
from .serializer import RoomSerializer, UserSerializer
from rest_framework import generics, status, mixins
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.permissions import IsAuthenticated, AllowAny
from django.contrib.auth import get_user_model


class CreateUserView(generics.CreateAPIView):

    model = get_user_model()
    permission_classes = [
        AllowAny
    ]
    serializer_class = UserSerializer


class GenericPropertyList(generics.GenericAPIView, mixins.ListModelMixin,
                          mixins.CreateModelMixin):
    serializer_class = PropertySerializer

    queryset = Property.objects.all()
    lookup_field = "id"

    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get(self, request):
        return self.list(request)

    def post(self, request):
        return self.create(request)


class GenericPropertyDetail(generics.GenericAPIView, mixins.ListModelMixin,
                            mixins.CreateModelMixin, mixins.UpdateModelMixin,
                            mixins.DestroyModelMixin,
                            mixins.RetrieveModelMixin):
    serializer_class = PropertySerializer
    queryset = Property.objects.all()

    lookup_field = "id"

    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, id=None):
        if id:
            return self.retrieve(request, id)
        else:
            return self.list(request)

    def put(self, request, id=None):
        property_instance = Property.objects.get(id=id)
        if request.user.id == property_instance.user_id:
            return self.update(request, id)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)

    def delete(self, request, id=None):
        property_instance = Property.objects.get(id=id)
        if request.user.id == property_instance.user_id:
            return self.destroy(request, id)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)


class GenericReservationList(generics.GenericAPIView, mixins.ListModelMixin):
    serializer_class = ReservationSerializer

    lookup_field = 'id'

    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        id = self.kwargs.get('id')
        if id:
            queryset = Reservation.objects.filter(prop_id=id)
        else:
            queryset = Reservation.objects.all()
        return queryset

    def get(self, request, id=None):
        return self.list(request, id)


class GenericReservationCreate(generics.GenericAPIView,
                               mixins.CreateModelMixin, mixins.ListModelMixin):

    lookup_field = 'id'

    authentication_classes = [SessionAuthentication, BasicAuthentication]

    serializer_class = ReservationSerializer

    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        id = self.kwargs.get("id")
        queryset = Reservation.objects.filter(prop_id=id)
        return queryset

    def get(self, request, id=None):
        return self.list(request, id)

    def post(self, request, id=None):
        return self.create(request, id)


class GenericReservationDetail(generics.GenericAPIView, mixins.ListModelMixin,
                               mixins.UpdateModelMixin,
                               mixins.DestroyModelMixin,
                               mixins.RetrieveModelMixin):

    serializer_class = ReservationSerializer
    queryset = Reservation.objects.all()

    lookup_field = "id"

    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, id=None):
        if id:
            return self.retrieve(request, id)
        else:
            return self.list(request)

    def put(self, request, id=None):
        reservation_instance = Reservation.objects.get(id=id)
        if request.user.id == reservation_instance.user_id:
            return self.update(request, id)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)

    def delete(self, request, id):
        reservation = Reservation.objects.get(id=id)
        if request.user.id == reservation.user_id:
            return self.destroy(request, id)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)


class GenericRoomList(generics.GenericAPIView, mixins.ListModelMixin):
    serializer_class = RoomSerializer

    lookup_field = 'id'

    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        id = self.kwargs.get('id')
        if id:
            queryset = Room.objects.filter(prop_id=id)
        else:
            queryset = Room.objects.all()
        return queryset

    def get(self, request, id=None):
        return self.list(request, id)


class GenericRoomCreate(generics.GenericAPIView, mixins.CreateModelMixin,
                        mixins.ListModelMixin):

    lookup_field = 'id'

    authentication_classes = [SessionAuthentication, BasicAuthentication]

    serializer_class = RoomSerializer

    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        id = self.kwargs.get("id")
        queryset = Room.objects.filter(prop_id=id)
        return queryset

    def get(self, request, id=None):
        return self.list(request, id)

    def post(self, request, id=None):
        property_instance = Property.objects.get(id=id)
        if property_instance.user_id == request.user.id:
            return self.create(request, id)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)


class GenericRoomDetail(generics.GenericAPIView, mixins.ListModelMixin,
                        mixins.UpdateModelMixin, mixins.DestroyModelMixin,
                        mixins.RetrieveModelMixin):

    serializer_class = RoomSerializer
    queryset = Room.objects.all()

    lookup_field = "id"

    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, id=None):
        if id:
            return self.retrieve(request, id)
        else:
            return self.list(request)

    def put(self, request, id=None):
        room_instance = Room.objects.get(id=id)
        property_instance = room_instance.prop
        if property_instance.user_id == request.user.id:
            return self.update(request, id)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)

    def delete(self, request, id):
        room_instance = Room.objects.get(id=id)
        property_instance = room_instance.prop
        if property_instance.user_id == request.user.id:
            return self.delete(request, id)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)
