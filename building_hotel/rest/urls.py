from django.urls import path
from . import views

app_name = 'rest'

urlpatterns = [
    path('signup/', views.CreateUserView.as_view(), name='signup'),
    path('properties', views.GenericPropertyList.as_view(), name='properties_list'),
    path('properties/<int:id>', views.GenericPropertyDetail.as_view(), name='properties_detail'),
    path('reservations', views.GenericReservationList.as_view(), name='reservation_list'),
    path('properties/<int:id>/reservations/', views.GenericReservationCreate.as_view(), name='reservation_list_by_property'),
    path('reservations/<int:id>', views.GenericReservationDetail.as_view(), name='reservation_detail'),
    path('rooms', views.GenericRoomList.as_view(), name='room_list'),
    path('properties/<int:id>/rooms', views.GenericRoomCreate.as_view(), name='rooms_by_property'),
    path('rooms/<int:id>', views.GenericRoomDetail.as_view(), name='room_detail'),
]