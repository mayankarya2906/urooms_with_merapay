from django.contrib import admin
from .models import ContactDetail

admin.site.register(ContactDetail)

# Register your models here.
