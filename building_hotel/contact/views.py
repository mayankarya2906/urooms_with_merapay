from django.shortcuts import render, redirect
from .models import ContactDetail
from .forms import ContactForm
from django.core.mail import send_mail as sm
from django.core.mail import BadHeaderError
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib import messages

@login_required(login_url="/accounts/login/")
def send_mail(request):
    contact_detail = ContactDetail.objects.last()
    if request.method == 'POST':
        contact_form = ContactForm(request.POST)
        if contact_form.is_valid():
            subject = contact_form.cleaned_data['subject']
            from_email = contact_form.cleaned_data['from_email']
            message = contact_form.cleaned_data['message']
            try:
                sm(subject, message, from_email, ['mayankarya2906@gmail.com'])

            except BadHeaderError:
                HttpResponse('Invalid Header!')

        return redirect('contact:success')

    else:
        contact_form = ContactForm()

    templates = 'contact/contact.html'

    context = {'contact_detail': contact_detail,
               'contact_form': contact_form}
    messages.success(request, 'hello')
    return render(request, templates, context)


@login_required(login_url="/accounts/login/")
def success(request):
    return HttpResponse('Message sent successfully!')
